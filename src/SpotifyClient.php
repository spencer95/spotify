<?php

namespace Drupal\spotify;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;

class SpotifyClient {

  /**
   * @var \GuzzleHttp\Client
   */
  protected $authorisationClient;

  /**
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The spotify authorisation token.
   *
   * @var string
   */
  protected $authorisationToken;

  /**
   * A configuration object containing spotify settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * SpotifyClient constructor.
   *
   * @param $http_client_factory \Drupal\Core\Http\ClientFactory
   */
  public function __construct($http_client_factory, ConfigFactoryInterface $config_factory) {

    $this->config = $config_factory->get('spotify.spotifysettings');

    // Get authorisation token.
    $this->authorisationClient = $http_client_factory->fromOptions([
      'verify' => FALSE,
    ]);

    $authorisation_response = $this->authorisationClient->post('https://accounts.spotify.com/api/token', [
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode($this->config->get('key') . ':' . $this->config->get('secret')),
      ],
      'form_params' => [
        'grant_type'=> 'client_credentials',
      ],
    ]);

    // Decode the authorisation response and assign the authorisation token.
    $authorastion_response_object = json_decode($authorisation_response->getBody()->getContents());
    $this->authorisationToken = $authorastion_response_object->access_token;

    // Setup client for Spotify API calls.
    $this->client = $http_client_factory->fromOptions([
      'base_uri' => 'https://api.spotify.com/v1/',
      'headers' => [
        'Authorization' => 'Bearer ' . $this->authorisationToken,
      ],
    ]);
  }

  /**
   * Get artists related to the given artist ID.
   *
   * @param int $artist_id
   *  Spotify artist ID.
   *
   * @return array
   *  Array of related artists.
   */
  public function getRelatedArtists($artist_id = '43ZHCT0cAZBISjO8DG9PnE') {
    $response = $this->client->get('artists/43ZHCT0cAZBISjO8DG9PnE/related-artists');
    return Json::decode($response->getBody());
  }

  /**
   * Get artist information from given artist ID.
   *
   * @param int $artist_id
   *  Spotify artist ID.
   *
   * @return array
   *  A Spotify artist object.
   */
  public function getArtist($artist_id = '43ZHCT0cAZBISjO8DG9PnE') {
    $response = $this->client->get("artists/$artist_id");
    return Json::decode($response->getBody());
  }

}
