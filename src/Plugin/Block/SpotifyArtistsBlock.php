<?php

namespace Drupal\spotify\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;



/**
 * Provides a 'SpotifyArtistsBlock' block.
 *
 * @Block(
 *  id = "spotify_artists_block",
 *  admin_label = @Translation("Spotify artists block"),
 * )
 */
class SpotifyArtistsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $spotifyClient;

  /**
   * SpotifyArtistsBlock constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param $spotify_client \Drupal\spotify\SpotifyClient
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $spotify_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->spotifyClient = $spotify_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('spotify_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'number_of_artists' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['number_of_artists'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of artists'),
      '#default_value' => $this->configuration['number_of_artists'],
      '#weight' => '0',
      '#min' => '0',
      '#max' => '20',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['number_of_artists'] = $form_state->getValue('number_of_artists');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $related_artists = $this->spotifyClient->getRelatedArtists('43ZHCT0cAZBISjO8DG9PnE');
    $items = [];

    foreach ($related_artists['artists'] as $key => $value) {
      // Limit the number of items to the value set in the config form.
      if ($key > ($this->configuration['number_of_artists'] -1)) {
        break;
      }

      $items[] = [
        '#title' => $value['name'],
        '#type' => 'link',
        '#url' => Url::fromRoute('spotify.spotify_artist_controller_displayArtistPage', ['spotify_artist_id' => $value['id']]),
      ];
    }

    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }

}
