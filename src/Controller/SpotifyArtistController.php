<?php

namespace Drupal\spotify\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SpotifyArtistController.
 */
class SpotifyArtistController extends ControllerBase {

  /**
   * Drupal\spotify\SpotifyClient definition.
   *
   * @var \Drupal\spotify\SpotifyClient
   */
  protected $spotifyClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->spotifyClient = $container->get('spotify_client');
    return $instance;
  }

  /**
   * Display artist page.
   *
   * @param string
   *  Spotify artist ID.
   *
   * @return array
   *   Return render array.
   */
  public function displayArtistPage($spotify_artist_id) {
    $artist_information = $this->spotifyClient->getArtist($spotify_artist_id);

    // Prepare artist genres as an unordered list.
    $artist_genres = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => 'Genres',
      '#items' => $artist_information['genres'],
    ];

    return [
      '#theme' => 'spotify_artist',
      '#name' => $artist_information['name'],
      '#popularity' => $artist_information['popularity'],
      '#genres' => $artist_genres,
      '#spotify_uri' => $artist_information['uri'],
      '#followers' => $artist_information['followers']['total'],
    ];
  }

  /**
   * Returns a page title.
   *
   * @param string
   *  Spotify artist ID.
   *
   * @return string
   *  Return page title string.
   */
  public function getTitle($spotify_artist_id) {
    $artist_information = $this->spotifyClient->getArtist($spotify_artist_id);
    return  $artist_information['name'];
  }

}
